package br.com.itau.icarros.questao4.service;

import java.util.List;

public interface UntilTheEndService {

	public Integer getWinnerTeamPosition(List<Integer> winMatchsQuantity, List<Integer> tieMatchsQuantity);

}
