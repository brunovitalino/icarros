package br.com.itau.icarros.questao4.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.springframework.stereotype.Service;

import br.com.itau.icarros.questao4.service.UntilTheEndService;

@Service
public class UntilTheEndServiceImpl implements UntilTheEndService {
	
	private final int WIN_POINT_VALUE = 3;
	private final int TIE_POINT_VALUE = 1;
	
	private Integer getTotalWinPoints(Integer matchQuantity) {
		return matchQuantity * WIN_POINT_VALUE;
	}
	
	private Integer getTotalTiePoints(Integer matchQuantity) {
		return matchQuantity * TIE_POINT_VALUE;
	}
	
	private Integer getTotalPoints(Integer winMatchsQuantity, Integer tieMatchsQuantity) {
		return getTotalWinPoints(winMatchsQuantity) + getTotalTiePoints(tieMatchsQuantity);
	}
	
	@Override
	public Integer getWinnerTeamPosition(List<Integer> teamsWinMatchsQuantity, List<Integer> teamsTieMatchsQuantity) {
		List<Integer> totalPoints = new ArrayList<>();
		IntStream.range(0, teamsWinMatchsQuantity.size())
				.forEach(i -> totalPoints.add(getTotalPoints(teamsWinMatchsQuantity.get(i), teamsTieMatchsQuantity.get(i))));
		return IntStream.range(0, totalPoints.size())
				.reduce((a,b) -> totalPoints.get(a) < totalPoints.get(b) ? b : a).getAsInt();
	}
	
	
}
