package br.com.itau.icarros.questao1.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.itau.icarros.questao1.service.FizzBuzzService;

@Service
public class FizzBuzzServiceImpl implements FizzBuzzService {

	@Override
	public List<Integer> loadNumbers() {
		List<Integer> numbers = new ArrayList<>();
		for (int n = 1; n <= 100; n++) {
			numbers.add(n);
		}
		return numbers;
	}

}
