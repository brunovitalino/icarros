package br.com.itau.icarros.questao1.service;

import java.util.List;

public interface FizzBuzzService {
	
	public List<Integer> loadNumbers();

}
