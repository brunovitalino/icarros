package br.com.itau.icarros.questao1.service.view;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.itau.icarros.questao1.service.MultipleService;

@Component
public class FizzBuzzView {
	
	@Autowired
	MultipleService ms;
	
	public void printFizzWhenMultipleOfThreeOrBuzzWhenMultipleOfFive(List<Integer> numbers) {
		numbers.stream().forEach(n -> {
			System.out.println(ms.isMultipleOfThreeAndFive(n) ? "FizzBuzz"
					: ms.isMultipleOfThree(n) ? "Fizz" : ms.isMultipleOfFive(n) ? "Buzz" : n);
		});
	}

}
