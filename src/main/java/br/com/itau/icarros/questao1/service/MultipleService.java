package br.com.itau.icarros.questao1.service;

public interface MultipleService {
	
	public boolean isMultipleOfThree(Integer n);
	
	public boolean isMultipleOfFive(Integer n);
	
	public boolean isMultipleOfThreeAndFive(Integer n);

}
