package br.com.itau.icarros.questao1.service.impl;

import org.springframework.stereotype.Service;

import br.com.itau.icarros.questao1.service.MultipleService;

@Service
public class MultipleServiceImpl implements MultipleService {
	
	@Override
	public boolean isMultipleOfThree(Integer n) {
		if (n % 3 == 0)
			return true;
		return false;
	}

	@Override
	public boolean isMultipleOfFive(Integer n) {
		if (n % 5 == 0)
			return true;
		return false;
	}

	@Override
	public boolean isMultipleOfThreeAndFive(Integer n) {
		if (isMultipleOfThree(n) && isMultipleOfFive(n)) {
			return true;
		}
		return false;
	}

}
