package br.com.itau.icarros.questao3.service;

import java.util.List;

public interface CreepHoursService {

	public Integer getClockBeepeds(List<String> results);
	
	public boolean isCreepHour(String moment);

}
