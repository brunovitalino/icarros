package br.com.itau.icarros.questao3.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.itau.icarros.questao3.service.CreepHoursService;

@Service
public class CreepHoursServiceImpl implements CreepHoursService {
	
	// Base Algorithms
	private boolean isAAXX(String m) {
		if (m.substring(0, 1).equals(m.substring(1, 2))) {
			return true;
		}
		return false;
	}
	
	private boolean isAXAX(String m) {
		if (m.substring(0, 1).equals(m.substring(2, 3))) {
			return true;
		}
		return false;
	}
	
	private boolean isAXXA(String m) {
		if (m.substring(0, 1).equals(m.substring(3))) {
			return true;
		}
		return false;
	}

	private boolean isXAAX(String m) {
		if (m.substring(1, 2).equals(m.substring(2, 3))) {
			return true;
		}
		return false;
	}

	private boolean isXAXA(String m) {
		if (m.substring(1, 2).equals(m.substring(3))) {
			return true;
		}
		return false;
	}

	private boolean isXXAA(String m) {
		if (m.substring(2, 3).equals(m.substring(3))) {
			return true;
		}
		return false;
	}

	// Intermediary Algorithms
	private boolean isAAAA(String moment) {
		return isAAXX(moment) && isXXAA(moment);
	}

	private boolean isABAB(String moment) {
		return isAXAX(moment) && isXAXA(moment);
	}

	private boolean isAABB(String moment) {
		return isAAXX(moment) && isXXAA(moment);
	}

	private boolean isABBA(String moment) {
		return isAXXA(moment) && isXAAX(moment);
	}

	// Final Algorithms
	@Override
	public Integer getClockBeepeds(List<String> moments) {
		return Long.valueOf(moments.stream()
					.filter(m -> isCreepHour(m))
					.count()
				).intValue();
	}

	@Override
	public boolean isCreepHour(String moment) {
		moment = removeColon(moment);
		return isAAAA(moment) || isABAB(moment)
				|| isAABB(moment) || isABBA(moment);
	}

	private String removeColon(String moment) {
		return Arrays.asList(moment.split(":")).stream()
				.reduce("", (totalContat, e) -> totalContat + e);
	}
	
}
