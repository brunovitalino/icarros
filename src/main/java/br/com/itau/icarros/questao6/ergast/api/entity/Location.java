package br.com.itau.icarros.questao6.ergast.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Location {

	@JsonProperty("lat")
	Double latitude;
	@JsonProperty("long")
	Double longitude;
	String locality;
	String country;

}
