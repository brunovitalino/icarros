package br.com.itau.icarros.questao6.ergast.api.entity;

import lombok.Data;

@Data
public class Speed {

	String units;
	String speed;

}
