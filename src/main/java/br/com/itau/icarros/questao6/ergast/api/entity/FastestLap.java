package br.com.itau.icarros.questao6.ergast.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class FastestLap {

	String rank;
	String lap;
	Time Time;
	@JsonProperty("AverageSpeed")
	Speed averageSpeed;

}
