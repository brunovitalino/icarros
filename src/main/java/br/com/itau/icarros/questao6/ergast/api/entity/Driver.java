package br.com.itau.icarros.questao6.ergast.api.entity;

import java.time.LocalDate;

import lombok.Data;

@Data
public class Driver {

	String driverId;
	Integer permanentNumber;
	String code;
	String url;
	String givenName;
	String familyName;
	LocalDate dateOfBirth;
	String nationality;

}
