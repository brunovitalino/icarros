package br.com.itau.icarros.questao6.ergast.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Circuit {

	String circuitId;
	String url;
	String circuitName;
	@JsonProperty("Location")
	Location location;

}
