package br.com.itau.icarros.questao6.ergast.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Formula1 {

	@JsonProperty("MRData")
	private MRData mRData;

}
