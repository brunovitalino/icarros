package br.com.itau.icarros.questao6.ergast.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class MRData {

	private String xmlns;
	private String series;
	private String url;
	private Integer limit;
	private Integer offset;
	private Integer total;
	@JsonProperty("RaceTable")
	private RaceTable raceTable;

}
