package br.com.itau.icarros.questao6.ergast.api.entity;

import lombok.Data;

@Data
public class Constructor {

	String constructorId;
	String url;
	String name;
	String nationality;

}
