package br.com.itau.icarros.questao6.ergast.api.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class RaceTable {

	String season;
	@JsonProperty("Races")
	List<Race> races;
	
}
