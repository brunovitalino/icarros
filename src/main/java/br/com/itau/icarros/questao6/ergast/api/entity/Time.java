package br.com.itau.icarros.questao6.ergast.api.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class Time {

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "SSSSSSSS")
	Date millis;
	String time;

}
