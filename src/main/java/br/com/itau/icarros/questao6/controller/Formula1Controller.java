package br.com.itau.icarros.questao6.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.icarros.questao6.ergast.api.entity.Formula1;
import br.com.itau.icarros.questao6.ergast.api.service.Formula1Service;

@RestController
@RequestMapping("/formula1")
public class Formula1Controller {
	
	@Autowired
	Formula1Service f1Service;

	@GetMapping("/year/{year}/results")
	public ResponseEntity<Formula1> loadAllRaceResultsByYear(@PathVariable Integer year) {
		Optional<Formula1> f1 = Optional.empty();
		try {
			f1 = Optional.ofNullable(f1Service.loadAllRaceResultsByYearBlocking(year));
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity().build();
		}
		if (f1.isEmpty())
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok(f1.get());
	}
}
