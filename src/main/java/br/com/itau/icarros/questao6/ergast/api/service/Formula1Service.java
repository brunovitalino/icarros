package br.com.itau.icarros.questao6.ergast.api.service;

import br.com.itau.icarros.questao6.ergast.api.entity.Formula1;
import reactor.core.publisher.Mono;

public interface Formula1Service {

	public Formula1 loadAllRaceResultsByYear(Integer year) throws Exception;

	public Mono<Formula1> loadAllRaceResultsByYearReactive(Integer year) throws Exception;

	public Formula1 loadAllRaceResultsByYearBlocking(Integer year) throws Exception;
		
}
