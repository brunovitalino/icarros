package br.com.itau.icarros.questao6.ergast.api.entity;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Race {

	Integer season;
	Integer round;
	String url;
	String raceName;
	@JsonProperty("Circuit")
	Circuit circuit;
	LocalDate date;
	String time;
	@JsonProperty("Results")
	List<Result> results;

}
