package br.com.itau.icarros.questao6.config.resttemplate;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class CustomClientHttpRequestInterceptor implements ClientHttpRequestInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomClientHttpRequestInterceptor.class);
    
	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {
        logRequestDetails(request);
        return execution.execute(request, body);
	}

	private void logRequestDetails(HttpRequest request) {
		LOGGER.debug("Executing external API...");
		LOGGER.debug("Headers: {}", request.getHeaders());
		LOGGER.debug("Request Method: {}", request.getMethod());
		LOGGER.debug("Request URI: {}", request.getURI());
	}

}
