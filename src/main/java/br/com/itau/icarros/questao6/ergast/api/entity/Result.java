package br.com.itau.icarros.questao6.ergast.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Result {

	Integer number;
	Integer position;
	String positionText;
	Integer points;
	@JsonProperty("Driver")
	Driver driver;
	@JsonProperty("Constructor")
	Constructor constructor;
	Integer grid;
	Integer laps;
	String status;
	@JsonProperty("Time")
	Time time;
	@JsonProperty("FastestLap")
	FastestLap fastestLap;

}
