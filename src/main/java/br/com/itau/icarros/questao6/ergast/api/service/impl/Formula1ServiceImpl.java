package br.com.itau.icarros.questao6.ergast.api.service.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import br.com.itau.icarros.questao6.ergast.api.entity.Formula1;
import br.com.itau.icarros.questao6.ergast.api.service.Formula1Service;
import reactor.core.publisher.Mono;

@Service
public class Formula1ServiceImpl implements Formula1Service {
	private final String API_PATH = "https://ergast.com/api";
	private final String SERVICE_PATH = "/f1";
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	WebClient webClientErgast;

	@Override
	public Formula1 loadAllRaceResultsByYear(Integer year) throws Exception {
		String path = "/"+year+"/results.json";
		String url = API_PATH + SERVICE_PATH + path;
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		HttpEntity<Formula1> f1Entity = new HttpEntity<>(headers);
		ResponseEntity<Formula1> response = restTemplate.exchange(url, HttpMethod.GET, f1Entity, Formula1.class);
		return response.getBody();
	}
	
	@Override
	public Mono<Formula1> loadAllRaceResultsByYearReactive(Integer year) throws Exception {
		String path = "/{year}/results.json";
		Map<String, String> params = new HashMap<>();
		params.put("year", year.toString());
		String url = SERVICE_PATH + path;
		Mono<Formula1> monoWebClient = webClientErgast
				.get()
				.uri(url, params)
				.retrieve()
				.bodyToMono(Formula1.class)
		        .onErrorMap(Exception::new);
		return monoWebClient;
	}
	
	@Override
	public Formula1 loadAllRaceResultsByYearBlocking(Integer year) throws Exception {
		return loadAllRaceResultsByYearReactive(year).share().block();
	}
	
}
