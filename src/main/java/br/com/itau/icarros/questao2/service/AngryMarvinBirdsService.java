package br.com.itau.icarros.questao2.service;

import java.util.List;

public interface AngryMarvinBirdsService {

	public Integer getNumberOfStars(List<String> results);

}
