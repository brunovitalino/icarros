package br.com.itau.icarros.questao2.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.itau.icarros.questao2.service.AngryMarvinBirdsService;

@Service
public class AngryMarvinBirdsServiceImpl implements AngryMarvinBirdsService {
	
	@Override
	public Integer getNumberOfStars(List<String> results) {
		return results.stream()
				.map(r -> Arrays.asList(r.split("-")).stream()
						.reduce("", (total, e) -> total + e).length())
				.reduce(0, (total, e) -> total + e);
	}

}
