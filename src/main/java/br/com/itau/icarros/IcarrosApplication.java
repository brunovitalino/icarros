package br.com.itau.icarros;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import br.com.itau.icarros.questao6.config.resttemplate.CustomClientHttpRequestInterceptor;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class IcarrosApplication {

    @Bean
	public RestTemplate restTemplate() {
    	RestTemplate restClient = new RestTemplate(
				new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));
		restClient.setInterceptors(Collections.singletonList(new CustomClientHttpRequestInterceptor()));
        return restClient;
    }
	
	@Bean
	public WebClient webClientErgast(WebClient.Builder builder) {
		String ErgastUrl = "https://ergast.com/api";
		return builder
				  .baseUrl(ErgastUrl)
				  .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE,
						  HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
				  .build();
	}

	public static void main(String[] args) {
		SpringApplication.run(IcarrosApplication.class, args);
	}

}
