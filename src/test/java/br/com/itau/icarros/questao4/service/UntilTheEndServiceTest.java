package br.com.itau.icarros.questao4.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class UntilTheEndServiceTest {
	
	@Autowired
	UntilTheEndService untilTheEndService;

	@Test
	void testSuccessGetWinnerTeamPosition() {
		List<Integer> wins = Arrays.asList(1, 0, 3);
		List<Integer> ties = Arrays.asList(2, 2, 0);
		assertEquals(2, untilTheEndService.getWinnerTeamPosition(wins, ties));
	}

}
