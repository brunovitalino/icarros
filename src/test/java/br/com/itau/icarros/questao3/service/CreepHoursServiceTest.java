package br.com.itau.icarros.questao3.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class CreepHoursServiceTest {
	
	@Autowired
	CreepHoursService creepHoursService;

	@Test
	void testSuccessGetClockBeepeds() {
		List<String> moments = Arrays.asList("11:00", "13:13", "10:00", "10:10", "01:01");
		assertEquals(4, creepHoursService.getClockBeepeds(moments));
	}

}
