create table Product(
	id BIGSERIAL NOT NULL,
	name VARCHAR(25) NOT NULL UNIQUE,
	price DECIMAL,
	CONSTRAINT product_pkey PRIMARY KEY (id)
);

create table Customer(
	id BIGSERIAL NOT NULL,
	name VARCHAR(25) NOT NULL UNIQUE,
	CONSTRAINT customer_pkey PRIMARY KEY (id)
);

create table "order"(
	id BIGSERIAL NOT NULL,
	product_id INT NOT NULL,
	quantity INT NOT NULL,
	customer_id INT NOT NULL,
	CONSTRAINT order_pkey PRIMARY KEY (id),
	FOREIGN KEY(product_id) REFERENCES Product(id),
	FOREIGN KEY(customer_id) REFERENCES Customer(id)
);

select o.id, p."name", p.price, o.quantity,
(p.price * o.quantity) as total_price, c."name"
from "order" o 
inner join product p on o.product_id = p.id 
inner join customer c on o.customer_id = c.id
where c."name" like 'DeathStar'
order by o.id, total_price;