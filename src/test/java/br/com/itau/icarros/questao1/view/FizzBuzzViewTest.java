package br.com.itau.icarros.questao1.view;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.itau.icarros.questao1.service.FizzBuzzService;
import br.com.itau.icarros.questao1.service.view.FizzBuzzView;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class FizzBuzzViewTest {
	
	@Autowired
	FizzBuzzService fizzBuzzService;
	
	@Autowired
	FizzBuzzView fizzBuzzView;

	@Test
	void testSuccessPrintFizzWhenMultipleOfThreeOrBuzzWhenMultipleOfFive() {
		List<Integer> numbers = fizzBuzzService.loadNumbers();
		fizzBuzzView.printFizzWhenMultipleOfThreeOrBuzzWhenMultipleOfFive(numbers);
	}

}
