package br.com.itau.icarros.questao1.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class FizzBuzzServiceTest {
	
	@Autowired
	FizzBuzzService fizzBuzzService;

	@Test
	void testSuccessLoadNumbers() {
		assertEquals(100, fizzBuzzService.loadNumbers().size());
	}

}
