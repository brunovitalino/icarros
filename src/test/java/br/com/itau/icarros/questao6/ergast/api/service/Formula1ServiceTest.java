package br.com.itau.icarros.questao6.ergast.api.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.itau.icarros.questao6.ergast.api.entity.Formula1;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class Formula1ServiceTest {
	
	@Autowired
	Formula1Service f1Service;

	@Test
	void testSuccessLoadAllRaceResultsByYear() {
		try {
			Integer year = 2020;
			Optional<Formula1> f1 = Optional.ofNullable(f1Service.loadAllRaceResultsByYear(year));
			assertTrue(f1.isPresent());
		} catch (Exception e) {
			fail("Não deveria ocorrer erro. " + e.getMessage());
		}
	}

	@Test
	void testSuccessLoadAllRaceResultsByYearBlocking() {
		try {
			Integer year = 2020;
			Optional<Formula1> f1 = Optional.ofNullable(f1Service.loadAllRaceResultsByYearBlocking(year));
			assertTrue(f1.isPresent());
		} catch (Exception e) {
			fail("Não deveria ocorrer erro. " + e.getMessage());
		}
	}

}
