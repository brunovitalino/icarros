package br.com.itau.icarros.questao6.controller;

import static org.junit.jupiter.api.Assertions.fail;

import java.net.URI;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
class Formula1ControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	void testSuccessLoadAllRaceResultsByYear() {
		Integer year = 2020;
		try {
			URI uri = new URI("/formula1/year/"+year.toString()+"/results");
			mockMvc.perform(
						MockMvcRequestBuilders
						.get(uri)
					).andExpect(
						MockMvcResultMatchers
						.status()
						.isOk()
					).andReturn();
		} catch (Exception e) {
			e.printStackTrace();
			fail("Código retornado não foi 200.");
		}
	}
}