package br.com.itau.icarros.questao2.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class AngryMarvinBirdsServiceTest {
	
	@Autowired
	AngryMarvinBirdsService resultsService;

	@Test
	void testSuccessGetNumberOfStars() {
		List<String> results = Arrays.asList("**--", "--*", "-*-", "*-*");
		assertEquals(6, resultsService.getNumberOfStars(results));
	}

}
