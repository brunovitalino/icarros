# icarros

iCarros API for accessing external APIs.

## Serviços

- Formula1;

## Sites

[Documentation](http://localhost:8080/swagger-ui.html)  
[Application](http://localhost:8080)

## Instructions to run locally

#### Running locally  

* JVM: `Java 11`  

Clone the project repository to a computer folder. Open the terminal or command prompt and execute the following steps:

1. Enter the project's root folder and type:  
``mvn clean package``  

2. Then, execute the project with the command:  
``java -jar target/icarros.jar``  

3. Call the desired service:  
`GET` [/formula1/year/2020/results](http://localhost:8080/formula1/year/2020/results) (example)  
